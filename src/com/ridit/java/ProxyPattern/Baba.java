package com.ridit.java.ProxyPattern;

public class Baba implements FamilyMember {
	private Mamma mamma;

	@Override
	public void cook() {
		// TODO Auto-generated method stub
		//lazy initialization
		System.out.println("Baba cannot cook so baba is giving proxy to mamma so baba will call mamma");
		mamma = new Mamma();
		mamma.cook();

	}

	public Mamma getMamma() {
		return mamma;
	}

	public void setMamma(Mamma mamma) {
		this.mamma = mamma;
	}

}
